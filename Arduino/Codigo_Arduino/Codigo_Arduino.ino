




#include <EtherCard.h>
#include <MFRC522.h>
#define SS_PIN 7
#define RST_PIN 9

MFRC522 rfid(SS_PIN, RST_PIN); // Instance of the class

MFRC522::MIFARE_Key key; 


static byte mymac[] = { 0x50,0xF1,0x2B,0x2A,0x30,0x31 };
byte Ethernet::buffer[700];
const char website[] PROGMEM = "192.168.0.3";
static byte hisip[] = { 192, 168, 0, 3 };
static uint32_t timer;
byte session;


//RFID
byte nuidPICC[7];
const int pinRESET= 3;



void setup() {
  pinMode(pinRESET, OUTPUT);

  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("\n[ThingSpeak example]");
    Serial.println("5");
    delay(1000);
    Serial.println("4");
    delay(1000);
    Serial.println("3");
    delay(1000);
        Serial.println("2");
        delay(1000);
    Serial.println("1");
    delay(1000);
    Serial.println("iniciando");

    


  initialize_ethernet();

  rfid.PCD_Init(); // Init MFRC522 

  for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
  }


}


void loop() {
  // put your main code here, to run repeatedly:
   ether.packetLoop(ether.packetReceive());

if ( ! rfid.PICC_IsNewCardPresent())
    return;

  // Verify if the NUID has been readed
  if ( ! rfid.PICC_ReadCardSerial())
    return;



    // Store NUID into nuidPICC array
    for (byte i = 0; i < 8; i++) {
      nuidPICC[i] = rfid.uid.uidByte[i];
    }
    char str[32] = "";
  
  array_to_string(nuidPICC, 4, str);

  for (byte i = 0; i < 32; i++) {
      if(str[i]==" "){
        str[i]="";
      }
    }
    

  // Halt PICC
  rfid.PICC_HaltA();

  // Stop encryption on PCD
  rfid.PCD_StopCrypto1();
    ether.printIp("My IP: ", ether.myip);
 
  //if (millis() > timer) {
    timer = millis() + 5000;
    Serial.println();
    Serial.print("<<< REQ ");

    ether.browseUrl(PSTR("/modelos/insertarTarjeta.php?uid="), str, website, my_callback);
    Serial.println("Paso ether");

  //}
   /* ether.printIp("Pinging: ", ether.hisip);
    ether.clientIcmpRequest(ether.hisip);*/


}

void initialize_ethernet(void){  
  for(;;){ // keep trying until you succeed 

digitalWrite(3, LOW); //FUNCIONA NORMAL
delay(1000);
digitalWrite(3, HIGH); //FUNCIONA NORMAL
    if (ether.begin(sizeof Ethernet::buffer, mymac, 5) == 0){ 
      Serial.println( "Failed to access Ethernet controller");
      continue;
    }
    
    if (!ether.dhcpSetup()){
      Serial.println("DHCP failed");
      continue;
    }

    ether.printIp("IP:  ", ether.myip);
    ether.printIp("GW:  ", ether.gwip);  
    ether.printIp("DNS: ", ether.dnsip);  

    ether.copyIp(ether.hisip, hisip);

      
    ether.printIp("SRV: ", ether.hisip);

   
  

    //reset init value
    break;  
  }
}
static void my_callback (byte status, word off, word len) {
  Serial.println(">>>");
  Ethernet::buffer[off+300] = 0;
  Serial.print((const char*) Ethernet::buffer + off);
  Serial.println("...");
}
void printHex(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
  }
}

/**
 * Helper routine to dump a byte array as dec values to Serial.
 */
void printDec(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], DEC);
  }
}

void array_to_string(byte array[], unsigned int len, char buffer[])
{
    for (unsigned int i = 0; i < len; i++)
    {
        byte nib1 = (array[i] >> 4) & 0x0F;
        byte nib2 = (array[i] >> 0) & 0x0F;
        buffer[i*2+0] = nib1  < 0xA ? '0' + nib1  : 'A' + nib1  - 0xA;
        buffer[i*2+1] = nib2  < 0xA ? '0' + nib2  : 'A' + nib2  - 0xA;
    }
    buffer[len*2] = '\0';
}
