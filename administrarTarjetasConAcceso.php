

<!DOCTYPE html>


<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<head>
<meta charset="UTF-8">
<title>Administrar tarjetas</title>
</head>
<link rel="stylesheet"
	href="bootstrap/css/bootstrap.min.css"></link>
<link href="estilos/principal.css" rel="stylesheet" type="text/css"></link>



<body>
	<header>
		<div class="container">
			<h1>Gestor de acceso</h1>
		</div>
	</header>

	<div class="container text-center">
		<h3>Tarjetas aprobadas</h3>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-10 offset-md-1 ">
				<table class="table table-striped table-bordered table-hover table-condensed text-center">

					<tr class="table-primary">
						<th>Numero de serie</th>
					</tr>
					<?php
					require("BaseDatos.class");
					$aprobadas = BaseDatos::getSingletonInstance()->getTarjetasAprobadas();
					for ($i=0; $i < count($aprobadas) ; $i++) {
							echo "<tr><td> $aprobadas[$i] </tr></td>";
					}
					 ?>
				</table>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2 ">
				<button class="btn btn-primary form-control"
				onclick="location.href='index.php'">Volver</button>
			</div>
		</div>
	</div>


	<script src="bootstrap/js/jquery.js"></script>
	<script src="bootstrap/js/bootstrap.min.js">
</body>

<script>
	var btn1 = document.getElementById("test1");
	var btn2 = document.getElementById("test2");

	btn1.addEventListener("click", function(ev) {
		alert('btn 1 clicked');
		ev.stopPropagation();
	}, true);

	btn2.addEventListener("click", function(ev) {
		alert('btn 2 clicked');
		ev.stopPropagation();
	}, true);
</script>
</html>
