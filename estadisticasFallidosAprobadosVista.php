
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta charset="UTF-8">
	<?php
	require("BaseDatos.class");
	$exitosos=BaseDatos::getSingletonInstance()->getIngresosExitosos();
	$fallidos=BaseDatos::getSingletonInstance()->getIngresosFallidos();



	?>
</head>
<link rel="stylesheet"
href="bootstrap/css/bootstrap.min.css"></link>
<link href="estilos/principal.css" rel="stylesheet" type="text/css"></link>

<body>

	<header>
		<div class="container">
			<h1>Gestor de acceso</h1>
		</div>
	</header>

	<div class="form-group">
		<div class="container text-center">
			<div class="col-md-12 offset-md-0" id="canvas-container">
				<canvas id="chart"></canvas>
			</div>


		</div>
		<div class="container text-center">
			<?php

			echo "Exitosos: $exitosos ";
			echo "Fallidos: $fallidos";
			?>
		</div>

	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2 ">
				<button class="btn btn-primary form-control"
				onclick="location.href='index.php'">Volver</button>
			</div>
		</div>
	</div>



	<script src="bootstrap/js/jquery.js"></script>
	<script src="dist/Chart.bundle.min.js"></script>

	<script>
	$(document).ready(function() {
		var datos = {
			type : "pie",
			data : {
				datasets : [ {
					data : [ <?php echo $exitosos; ?>, <?php echo $fallidos; ?>,
					],
					backgroundColor : [ "#87cefa", "#3e5f8a", ],
				} ],
				labels : [ "Validos", "Fallidos", ],
			},
			options : {
				responsive : true,
			}
		};

		var canvas = document.getElementById('chart').getContext('2d');
		window.pie = new Chart(canvas, datos);

	});
	</script>
	<script src="bootstrap/js/bootstrap.min.js"></script>


</body>
</html>
