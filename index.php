
<!DOCTYPE html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php
require("BaseDatos.class");
$ip = BaseDatos::getSingletonInstance()->getIpArduino();


?>
<html>
<head>
	<meta charset="UTF-8">
	<title>Gestor de acceso</title>
</head>
<link rel="stylesheet"
href="bootstrap/css/bootstrap.min.css"></link>
<link href="estilos/principal.css" rel="stylesheet" type="text/css"></link>

<body>

	<header>
		<div class="container">
			<h1>Gestor de acceso</h1>
		</div>
	</header>
	<div class="container">
		<p> Ip arduino: <?php echo $ip ?></p>
	</div>

	<div class="container">
		<div class="row">
			<div class="color1 col-md-4 p-3 text-center">
				<p>Desea conocer cuantas tarjetas se han utilizado en la entrada
					del edificio en el ultimo mes?</p>
					<button class="btn btn-primary" onclick="location.href='cantidadTarjetasUtilizadas.php'">Acceder</button>
			</div>
			<div class="color2  col-md-4 p-3 text-center">
				<p>Grafico de las veces de los ingresos exitosos y fallidos al edificio</p>
					<button class="btn btn-primary"
					onclick="location.href='estadisticasFallidosAprobadosVista.php'">
					Acceder</button>
			</div>
				<div class="color1 col-md-4 p-3  text-center">
					<p>Tarjetas que pueden acceder al edificio actualmente</p>
					<button class="btn btn-primary" onclick="location.href='administrarTarjetasConAcceso.php'">Acceder</button>
				</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="color2 col-md-4 p-4 text-center">
				<p>Vista general de las tarjetas</p>
					<button class="btn btn-primary" onclick="location.href='vistaGeneral.php'">Acceder</button>
			</div>
		</div>
	</div>

				<script src="bootstrap/js/jquery.js"></script>
				<script src="bootstrap/js/bootstrap.min.js">

				</script>
			</body>
			</html>
