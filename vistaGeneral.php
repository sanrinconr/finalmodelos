
<!DOCTYPE html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php
require("BaseDatos.class");
$ip = BaseDatos::getSingletonInstance()->getIpArduino();


?>
<html>
<head>
  <meta charset="UTF-8">
  <title>Gestor de acceso</title>
</head>
<link rel="stylesheet"
href="bootstrap/css/bootstrap.min.css"></link>
<link href="estilos/principal.css" rel="stylesheet" type="text/css"></link>

<body>

  <header>
    <div class="container">
      <h1>Gestor de acceso</h1>
    </div>
  </header>

  <div class="container text-center ">
    <h3>Vista General</h3>

  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12 ">
        <table class="table table-striped table-bordered table-hover table-sm text-center">

          <tr class="table-primary">
            <th>Uid</th>
            <th>Estado</th>
            <th>Usos</th>
          </tr>
          <?php
          $usadas = BaseDatos::getSingletonInstance()->getUsosTarjetas();

          if($usadas[0][0] != NULL){
            for ($a=0; $a < count($usadas) ; $a++) {
              echo "<tr>";
              for ($i=0; $i < 3 ; $i++) {
                if ($usadas[$a][$i] != NULL)
                echo "<td> ".$usadas[$a][$i]."</td>";
                else
                break;
              }
              echo "</tr>";
            }
          }else{
            echo "<tr><td>SIN DATOS</td><td>SIN DATOS</td><td>SIN DATOS</td></tr>";
          }
          ?>
        </table>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-8 offset-md-2 ">
        <button class="btn btn-primary form-control"
        onclick="location.href='index.php'">Volver</button>
      </div>
    </div>
  </div>

  <script src="bootstrap/js/jquery.js"></script>
  <script src="bootstrap/js/bootstrap.min.js">

  </script>
</body>
</html>
